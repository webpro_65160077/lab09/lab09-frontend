import type { BuyStock } from '@/types/BuyStock'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import buyStockService from '@/services/buyStock'

export const useBuyStockStore = defineStore('buyStock', () => {
  const loadingStore = useLoadingStore()
  const material = ref<BuyStock[]>([])
  const search = ref<string>('')
  const initialBuyStock: BuyStock = {
    name: '',
    balance: 0,
    unit: '',
    cost: 0
  }
  const editedBuyStock = ref<BuyStock>(JSON.parse(JSON.stringify(initialBuyStock)))

  async function getBuyStocks() {
    // loadingStore.doLoad()
    const res = await buyStockService.getbuystocks()
    material.value = res.data
    // loadingStore.finish()
  }

  async function getBuyStock(id: number) {
    // loadingStore.doLoad()
    const res = await buyStockService.getBuyStock(id)
    editedBuyStock.value = res.data
    // loadingStore.finish()
  }

  async function saveBuyStock() {
    // loadingStore.doLoad()
    const buyStock = editedBuyStock.value
    if (!buyStock.id) {
      console.log('Post' + JSON.stringify(buyStock))
      const res = await buyStockService.addBuyStock(buyStock)
    } else {
      console.log('Patch' + JSON.stringify(buyStock))
      const res = await buyStockService.updateBuyStock(buyStock)
    }
    await getBuyStocks()
    // loadingStore.finish()
  }

  async function deleteBuyStock() {
    // loadingStore.doLoad()
    const buyStock = editedBuyStock.value
    const res = await buyStockService.delBuyStock(buyStock)
    await getBuyStocks()
    // loadingStore.finish()
  }

  const searchBuyStock = computed(() => {
    const searchTerm = search.value
    return material.value.filter((item) => {
      return item.name.includes(searchTerm)
    })
  })

  const totalBuyStocks = computed(() => material.value.length)
  function clearForm() {
    editedBuyStock.value = JSON.parse(JSON.stringify(initialBuyStock))
  }

  return {
    getBuyStocks,
    getBuyStock,
    saveBuyStock,
    deleteBuyStock,
    search,
    totalBuyStocks,
    searchBuyStock,
    editedBuyStock,
    clearForm
  }
})
